package payroll;

/**
 *
 * @author rushil
 */
public class PayrollSimulation {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        EmployeeFactory factory = EmployeeFactory.getInstance();
        Employee employee1 = factory.getEmployee(EmployeeTypes.EMPLOYEE, "Rushil", 20, 90, 2000);
        System.out.println(employee1.toString());
        
        Employee employee2 = factory.getEmployee(EmployeeTypes.EMPLOYEE, "Abcd", 30, 40, 3000);
        System.out.println(employee2.toString());
        
        Employee manager1 = factory.getEmployee(EmployeeTypes.MANAGER, "Qwerty", 24, 80, 1000);
        System.out.println(manager1.toString());
        
        Employee manager2 = factory.getEmployee(EmployeeTypes.MANAGER, "lkjhg", 18, 49, 2000);
        System.out.println(manager2.toString());
    }
    
}
