package payroll;

/**
 *
 * @author rushil
 */
public class EmployeeFactory {
    private static EmployeeFactory empFactory;
    
    private EmployeeFactory() {
        
    }
    
    public static EmployeeFactory getInstance() {
        if( empFactory == null ) {
            empFactory = new EmployeeFactory();
        }
        return empFactory;
    }
    
    public Employee getEmployee(EmployeeTypes type,String name, double hours, double hourlyWage, double bonus) {
          Employee employee = null;
        switch (type) {
            case EMPLOYEE :
                employee = new Employee(name,  hours,  hourlyWage);
            break;
            case MANAGER : 
                employee = new Manager( name,  hours,  hourlyWage, bonus);
                break;
        }
        return employee;
    }
}
