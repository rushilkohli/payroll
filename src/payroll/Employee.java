package payroll;

/**
 *
 * @author rushil
 */
public class Employee {
    private String name;
    private double hours;
    private double hourlyWage;
    
    public Employee() {
        
    }
    
    public Employee(String name, double hours, double hourlyWage) {
        this.name = name;
        this.hours = hours;
        this.hourlyWage = hourlyWage;
    }
    
    public String getName() {
        return name;
    }
    
    public double getHours() {
        return hours;
    }

    public double getHourlyWage() {
        return hourlyWage;
    }
    
    private double calculatePay() {
        return hours*hourlyWage;
    }
    
    @Override
    public String toString() {
        return ("Employee name: " + getName() + "\n" + "Pay income: " + calculatePay());
    }
}
