package payroll;

/**
 *
 * @author rushil
 */
public class Manager extends Employee {
    
    private double bonus;
    public Manager() {
        
    }
    
    public Manager(String name, double hours, double hourlyWage, double bonus) {
        super(name, hours, hourlyWage);
        this.bonus = bonus;
    }
        
    public double getBonus() {
        return bonus;
    }
    private double calculatePay() {
        return getHours()*getHourlyWage() + bonus;
    }
    
    @Override
    public String toString() {
        return ("Manager name: " + getName() + "\n" + "Pay income: " + calculatePay());
    }
}
